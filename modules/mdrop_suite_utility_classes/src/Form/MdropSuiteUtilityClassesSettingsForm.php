<?php

namespace Drupal\mdrop_suite_utility_classes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure mdrop suite utility classes settings.
 */
class MdropSuiteUtilityClassesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mdrop_suite_utility_classes_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mdrop_suite_utility_classes.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('mdrop_suite_utility_classes.settings');
    $form['utility_classes'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Utility classes'),
      '#description' => $this->t(
        'Using similar as <a href=":link" target="_blank">Bootstrap 5 spacing utilities</a>, you can adjust to fit your bootstrap customization or completly customized styles.',
        [':link' => 'https://getbootstrap.com/docs/5.0/utilities/spacing/#utilities-api']
      ),
    ];

    $form['utility_classes']['minimal'] = [
      '#type' => 'checkbox',
      '#disabled' => TRUE,
      '#title' => $this->t('Add minimal styles'),
      '#description' => $this->t('Make sure you manage minimal styles in your theme when disabled, or even templates if you want to use different classes approach. Do not enable if you are using a bootstrap 5 based theme.'),
      '#default_value' => $config->get('minimal'),
    ];

    $form['utility_classes']['container_class'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Container class'),
      '#description' => $this->t('Example "container".'),
      '#default_value' => $config->get('container_class'),
    ];

    $form['utility_classes']['spacers_count'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 10,
      '#required' => TRUE,
      '#title' => $this->t('Spacers count'),
      '#description' => $this->t('How many spacers variants whould you like to appliy to spacing related options (padding, margin, gap), 0 to 10 range.'),
      '#default_value' => $config->get('spacers_count'),
    ];

    $form['utility_classes']['spacers_padding_class_base'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Spacers padding class base'),
      '#description' => $this->t('Example "p". Then classes will be used as "[padding_class_base]-[spacer_number]" e.g: "p-1".'),
      '#default_value' => $config->get('spacers_padding_class_base'),
    ];

    $form['utility_classes']['spacers_margin_class_base'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Spacers margin class base'),
      '#description' => $this->t('Example "m". Then classes will be used as "[margin_class_base]-[spacer_number]" e.g: "m-1".'),
      '#default_value' => $config->get('spacers_margin_class_base'),
    ];

    $form['utility_classes']['spacers_x_class_base'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Spacers horizontal spacing class base (for paddings & margins)'),
      '#description' => $this->t('Example "x". Then classes will be used as "[padding_class_base][spacers_x_class_base]-[spacer_number]" e.g: "px-1".'),
      '#default_value' => $config->get('spacers_x_class_base'),
    ];

    $form['utility_classes']['spacers_y_class_base'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Spacers vertical spacing class base (for paddings & margins)'),
      '#description' => $this->t('Example "y". Then classes will be used as "[padding_class_base][spacers_y_class_base]-[spacer_number]" e.g: "py-1".'),
      '#default_value' => $config->get('spacers_y_class_base'),
    ];

    $form['utility_classes']['spacers_start_class_base'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Spacers start spacing class base (for paddings & margins)'),
      '#description' => $this->t('Example "s". Then classes will be used as "[padding_class_base][spacers_start_class_base]-[spacer_number]" e.g: "ps-1".'),
      '#default_value' => $config->get('spacers_start_class_base'),
    ];

    $form['utility_classes']['spacers_end_class_base'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Spacers end spacing class base (for paddings & margins)'),
      '#description' => $this->t('Example "e". Then classes will be used as "[padding_class_base][spacers_end_class_base]-[spacer_number]" e.g: "pe-1".'),
      '#default_value' => $config->get('spacers_end_class_base'),
    ];

    $form['utility_classes']['spacers_gutter_class_base'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Spacers gutter class base'),
      '#description' => $this->t('Example "g". Then classes will be used as "[gutter_class_base]-[spacer_number]" e.g: "g-1".'),
      '#default_value' => $config->get('spacers_gutter_class_base'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mdrop_suite_utility_classes.settings')
      ->set('minimal', $form_state->getValue('minimal'))
      ->set('container_class', $form_state->getValue('container_class'))
      ->set('spacers_count', $form_state->getValue('spacers_count'))
      ->set('spacers_padding_class_base', $form_state->getValue('spacers_padding_class_base'))
      ->set('spacers_margin_class_base', $form_state->getValue('spacers_margin_class_base'))
      ->set('spacers_x_class_base', $form_state->getValue('spacers_x_class_base'))
      ->set('spacers_y_class_base', $form_state->getValue('spacers_y_class_base'))
      ->set('spacers_start_class_base', $form_state->getValue('spacers_start_class_base'))
      ->set('spacers_end_class_base', $form_state->getValue('spacers_end_class_base'))
      ->set('spacers_gutter_class_base', $form_state->getValue('spacers_gutter_class_base'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
