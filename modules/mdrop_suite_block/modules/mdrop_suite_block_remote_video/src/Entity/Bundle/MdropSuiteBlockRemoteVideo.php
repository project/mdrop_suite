<?php

namespace Drupal\mdrop_suite_block_remote_video\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mdrop_suite_block\Entity\Bundle\MdropSuiteBlockBase;
use Drupal\mdrop_suite_bundle_field\BundleField\MdropSuiteBundleFieldRemoteVideoTrait;

/**
 * Bundle class for MdropSuiteBlockRemoteVideo.
 */
class MdropSuiteBlockRemoteVideo extends MdropSuiteBlockBase {

  const BUNDLE_KEY = 'mdrop_suite_remote_video';

  use MdropSuiteBundleFieldRemoteVideoTrait {
    bundleFieldDefinitions as bundleFieldDefinitionsRemoteVideo;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields += self::bundleFieldDefinitionsRemoteVideo($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
