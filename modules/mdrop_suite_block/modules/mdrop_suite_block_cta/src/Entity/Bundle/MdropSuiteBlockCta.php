<?php

namespace Drupal\mdrop_suite_block_cta\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mdrop_suite_block\Entity\Bundle\MdropSuiteBlockBase;
use Drupal\mdrop_suite_bundle_field\BundleField\MdropSuiteBundleFieldCtaTrait;

/**
 * Bundle class for MdropSuiteBlockCTA.
 */
class MdropSuiteBlockCta extends MdropSuiteBlockBase {

  const BUNDLE_KEY = 'mdrop_suite_cta';

  use MdropSuiteBundleFieldCtaTrait {
    bundleFieldDefinitions as bundleFieldDefinitionsCta;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields += self::bundleFieldDefinitionsCta($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
