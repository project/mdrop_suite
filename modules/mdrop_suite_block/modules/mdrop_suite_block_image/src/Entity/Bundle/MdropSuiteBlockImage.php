<?php

namespace Drupal\mdrop_suite_block_image\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mdrop_suite_block\Entity\Bundle\MdropSuiteBlockBase;
use Drupal\mdrop_suite_bundle_field\BundleField\MdropSuiteBundleFieldImageTrait;

/**
 * Bundle class for MdropSuiteBlockImage.
 */
class MdropSuiteBlockImage extends MdropSuiteBlockBase {

  const BUNDLE_KEY = 'mdrop_suite_image';

  use MdropSuiteBundleFieldImageTrait {
    bundleFieldDefinitions as bundleFieldDefinitionsImage;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields += self::bundleFieldDefinitionsImage($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
