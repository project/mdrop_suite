<?php

namespace Drupal\mdrop_suite_block_paragraph\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mdrop_suite_block\Entity\Bundle\MdropSuiteBlockBase;
use Drupal\mdrop_suite_bundle_field\BundleField\MdropSuiteBundleFieldParagraphTrait;

/**
 * Bundle class for MdropSuiteBlockParagraph.
 */
class MdropSuiteBlockParagraph extends MdropSuiteBlockBase {

  const BUNDLE_KEY = 'mdrop_suite_paragraph';

  use MdropSuiteBundleFieldParagraphTrait {
    bundleFieldDefinitions as bundleFieldDefinitionsParagraph;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields += self::bundleFieldDefinitionsParagraph($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
