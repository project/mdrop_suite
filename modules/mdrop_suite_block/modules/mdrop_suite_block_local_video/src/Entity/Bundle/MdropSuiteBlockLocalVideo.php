<?php

namespace Drupal\mdrop_suite_block_local_video\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mdrop_suite_block\Entity\Bundle\MdropSuiteBlockBase;
use Drupal\mdrop_suite_bundle_field\BundleField\MdropSuiteBundleFieldLocalVideoTrait;

/**
 * Bundle class for MdropSuiteBlockLocalVideo.
 */
class MdropSuiteBlockLocalVideo extends MdropSuiteBlockBase {

  const BUNDLE_KEY = 'mdrop_suite_local_video';

  use MdropSuiteBundleFieldLocalVideoTrait {
    bundleFieldDefinitions as bundleFieldDefinitionsLocalVideo;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields += self::bundleFieldDefinitionsLocalVideo($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
