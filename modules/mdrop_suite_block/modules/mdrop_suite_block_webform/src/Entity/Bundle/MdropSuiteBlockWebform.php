<?php

namespace Drupal\mdrop_suite_block_webform\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mdrop_suite_block\Entity\Bundle\MdropSuiteBlockBase;
use Drupal\mdrop_suite_bundle_field\BundleField\MdropSuiteBundleFieldWebformTrait;

/**
 * Bundle class for MdropSuiteBlockWebform.
 */
class MdropSuiteBlockWebform extends MdropSuiteBlockBase {

  const BUNDLE_KEY = 'mdrop_suite_webform';

  use MdropSuiteBundleFieldWebformTrait {
    bundleFieldDefinitions as bundleFieldDefinitionsWebform;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields += self::bundleFieldDefinitionsWebform($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
