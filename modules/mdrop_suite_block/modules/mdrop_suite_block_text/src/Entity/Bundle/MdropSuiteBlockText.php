<?php

namespace Drupal\mdrop_suite_block_text\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mdrop_suite_block\Entity\Bundle\MdropSuiteBlockBase;
use Drupal\mdrop_suite_bundle_field\BundleField\MdropSuiteBundleFieldTextTrait;

/**
 * Bundle class for MdropSuiteBlockText.
 */
class MdropSuiteBlockText extends MdropSuiteBlockBase {

  const BUNDLE_KEY = 'mdrop_suite_text';

  use MdropSuiteBundleFieldTextTrait {
    bundleFieldDefinitions as bundleFieldDefinitionsText;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields += self::bundleFieldDefinitionsText($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
