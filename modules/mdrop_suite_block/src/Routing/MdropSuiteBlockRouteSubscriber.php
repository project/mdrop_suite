<?php

namespace Drupal\mdrop_suite_block\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Override  controller.
 */
class MdropSuiteBlockRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('layout_builder.choose_block')) {
      $defaults = $route->getDefaults();
      $defaults['_controller'] = '\Drupal\mdrop_suite_block\Controller\MdropSuiteBlockChooseBlockController::build';
      $route->setDefaults($defaults);
    }
  }

}
