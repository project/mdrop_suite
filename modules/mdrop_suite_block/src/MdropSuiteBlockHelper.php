<?php

namespace Drupal\mdrop_suite_block;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Helper "MdropSuiteBlockHelper" service object.
 */
class MdropSuiteBlockHelper {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a "MdropSuiteBlockHelper" object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Remove block content of type & uninstall default related config,
   *
   * @param string $type_key
   *  Block content type.
   */
  public function uninstallBlockContentType($type_key) {
    $storage = $this->entityTypeManager ->getStorage('block_content');
    $blocks = $storage->loadByProperties(['type' => $type_key]);
    $storage->delete($blocks);

    $this->configFactory->getEditable('language.content_settings.block_content.' . $type_key)->delete();
    $this->configFactory->getEditable('core.entity_view_display.block_content.' . $type_key . '.default')->delete();
    $this->configFactory->getEditable('core.entity_form_display.block_content.' . $type_key . '.default')->delete();
    $this->configFactory->getEditable('block_content.type.' . $type_key)->delete();
  }

}
