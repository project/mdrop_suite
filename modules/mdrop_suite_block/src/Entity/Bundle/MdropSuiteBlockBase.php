<?php

namespace Drupal\mdrop_suite_block\Entity\Bundle;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\block_content\Entity\BlockContent;

/**
 * MdropSuiteBlockContentBase block content base class.
 */
abstract class MdropSuiteBlockBase extends BlockContent {

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    return $fields;
  }

}
