<?php

namespace Drupal\mdrop_suite_block\Controller;

use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_restrictions\Controller\ChooseBlockController as ChooseBlockControllerCore;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;

/**
 * Choose a new block.
 */
class MdropSuiteBlockChooseBlockController extends ChooseBlockControllerCore {

  /**
   * {@inheritdoc}
   */
  public function build(SectionStorageInterface $section_storage, $delta, $region) {
    $build = parent::build($section_storage, $delta, $region);
    $inline_build = parent::inlineBlockList($section_storage, $delta, $region);
    if (!empty($build['add_block'])) {
      $build['add_block']['#access'] = FALSE;
    }
    if (!empty($inline_build['back_button'])) {
      $inline_build['back_button']['#access'] = FALSE;
    }
    // Add wrapper to get filters working @see
    // https://www.drupal.org/project/drupal/issues/3308658#comment-15002205
    $build['#type'] = 'container';

    if (!empty($inline_build['links']) && !empty($inline_build['links']['#links'])) {
      $build['block_categories']['InlineBlocks'] = [
        '#type' => 'details',
        '#attributes' => ['class' => ['js-layout-builder-category']],
        '#open' => TRUE,
        '#title' => $this->t('Add new block'),
        'links' => $inline_build['links'],
        '#weight' => -200,
      ];
    }
    if (!empty($build['block_categories'])) {
      foreach (Element::children($build['block_categories']) as $category_key) {
        $build['block_categories'][$category_key]['#open'] = $category_key == 'InlineBlocks';
        $this->categoryBlockLinksAddIcon($build['block_categories'][$category_key], $category_key);
//        dpm($category_key);
      }
    }
    return $build;
  }

  /**
   * Generate block icons for category block links.
   *
   * @param array $category
   *   Categroty.
   */
  protected function categoryBlockLinksAddIcon(array &$category, $category_key) {
    if (!empty($category['links']) && !empty($category['links']['#links'])) {
      // Default block image.
      $icon_base_path = base_path() . \Drupal::service('extension.list.module')->getPath('mdrop_suite_block') . '/images/f5f5f2/';
      // @TODO!
      // $icon_url = \Drupal::service('file_url_generator')->generateString($icon_base_path . 'linuxfoundation.svg');
      $icon_url = $icon_base_path . 'grid.svg';

      foreach (Element::children($category['links']['#links']) as $delta) {
        if (!empty($category['links']['#links'][$delta]['url'])) {
          if ($category_key == 'InlineBlocks') {
            if ($category['links']['#links'][$delta]['url']->getRouteParameters()['plugin_id'] == 'inline_block:mdrop_suite_text') {
              $icon_url = $icon_base_path . 'pencil.svg';
            }
            elseif ($category['links']['#links'][$delta]['url']->getRouteParameters()['plugin_id'] == 'inline_block:mdrop_suite_image') {
              $icon_url = $icon_base_path . 'linuxfoundation.svg';
            }
            elseif ($category['links']['#links'][$delta]['url']->getRouteParameters()['plugin_id'] == 'inline_block:mdrop_suite_cta') {
              $icon_url = $icon_base_path . 'star.svg';
            }
            
            elseif ($category['links']['#links'][$delta]['url']->getRouteParameters()['plugin_id'] == 'inline_block:mdrop_suite_webform') {
              $icon_url = $icon_base_path . 'table.svg';
            }
            elseif ($category['links']['#links'][$delta]['url']->getRouteParameters()['plugin_id'] == 'inline_block:mdrop_suite_remote_video' || $category['links']['#links'][$delta]['url']->getRouteParameters()['plugin_id'] == 'inline_block:mdrop_suite_local_video') {
              $icon_url = $icon_base_path . 'youtube.svg';
            }
            elseif ($category['links']['#links'][$delta]['url']->getRouteParameters()['plugin_id'] == 'inline_block:mdrop_suite_paragraph') {
              $icon_url = $icon_base_path . 'puzzlepiece.svg';
            }
          }
          $label = $category['links']['#links'][$delta]['title'];
          $category['links']['#links'][$delta]['title'] = Markup::create('<img src="' . $icon_url . '" class="block-link-img" /> ' . '<span class="block-link-label">' . $label . '</span>');
        }
      }
    }
  }

}
