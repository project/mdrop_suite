<?php

namespace Drupal\mdrop_suite_media;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Helper "MdropSuiteMediaHelper" service object.
 */
class MdropSuiteMediaHelper {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a "MdropSuiteBlockHelper" object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Remove medias of bundle & uninstall default related config,
   *
   * @param string $bundle_key
   *  Media bundle key.
   */
  public function uninstallMediaBundle($bundle_key) {
    $storage = $this->entityTypeManager ->getStorage('media');
    $medias = $storage->loadByProperties(['bundle' => $bundle_key]);
    $storage->delete($medias);

    $this->configFactory->getEditable('core.entity_form_display.media.' . $bundle_key . '.default')->delete();
    $this->configFactory->getEditable('core.entity_form_display.media.' . $bundle_key . '.media_library')->delete();
    $this->configFactory->getEditable('core.entity_view_display.media.' . $bundle_key . '.default')->delete();
    $this->configFactory->getEditable('core.entity_view_display.media.' . $bundle_key . '.mdrop_suite_background')->delete();
    $this->configFactory->getEditable('core.entity_view_display.media.' . $bundle_key . '.media_library')->delete();
    $this->configFactory->getEditable('field.field.media.' . $bundle_key . '.field_media_image')->delete();
    $this->configFactory->getEditable('field.field.media.' . $bundle_key . '.field_media_video_file')->delete();
    $this->configFactory->getEditable('field.field.media.' . $bundle_key . '.field_media_oembed_video')->delete();
    $this->configFactory->getEditable('language.content_settings.media.' . $bundle_key)->delete();
    $this->configFactory->getEditable('media.type.' . $bundle_key . '')->delete();
  }

}
