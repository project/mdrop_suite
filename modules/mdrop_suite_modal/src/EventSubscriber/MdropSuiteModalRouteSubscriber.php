<?php

namespace Drupal\mdrop_suite_modal\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Drupal\Core\Routing\RouteMatch;
use Drupal\mdrop_suite_modal\MdropSuiteModalHelper;
use Drupal\Core\Asset\LibraryDiscoveryInterface;

/**
 * Provides Mdrop Suite - Modal event subscribers.
 */
class MdropSuiteModalRouteSubscriber implements EventSubscriberInterface {

  /**
   * Close modal into ajax response.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event.
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();

    if ($response instanceof AjaxResponse) {
      $close_dialog = FALSE;
      $commands = &$response->getCommands();
      foreach ($commands as $command) {
        if (isset($command['selector']) && $command['selector'] === '#layout-builder') {
          $close_dialog = TRUE;
          break;
        }
      }
      if ($close_dialog) {
        $response->addCommand(new CloseDialogCommand('#' . MdropSuiteModalHelper::DIALOG_TARGET));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => [['onResponse']],
    ];
  }

}
