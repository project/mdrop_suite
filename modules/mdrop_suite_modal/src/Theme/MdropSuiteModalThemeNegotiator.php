<?php

namespace Drupal\mdrop_suite_modal\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\mdrop_suite_modal\MdropSuiteModalHelper;
use Drupal\Core\Theme\AjaxBasePageNegotiator;

/**
 * Theme negotiator.
 */
class MdropSuiteModalThemeNegotiator extends AjaxBasePageNegotiator {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    // Inherit admin theme library definitions to have assets loaded before
    // modal opens, not using iframe due complexity & some limititations like
    // not having modal actions, auto height not working, ...
    // Also avoid replicate some specific theme styles, use originals.
    // This implementation consist:
    // 1. Build library dinamically with admin theme libraries as dependencies.
    // 2. Library discovery collector used to mix & ensure styles for lb page.
    // 3. Layout builder will attach dinamically generated library.
    // 4. Layout builder will toggle active theme by theme negotiator.
    // @see
    // MdropSuiteModalLayoutBuilderPreRender::preRender().
    // MdropSuiteModalLibraryDiscoveryCollector::getCid().
    // MdropSuiteModalLibraryDiscoveryCollector::resolveCacheMiss().
    $applies = parent::applies($route_match);
    $ajax_page_state = $this->requestStack->getCurrentRequest()->request->all('ajax_page_state');
    $route_name = $route_match->getRouteName();
    $route_name_condition = MdropSuiteModalHelper::layoutBuilderLayoutBuilderRouteNameCheck($route_name);

    if ($applies && $route_name_condition && MdropSuiteModalHelper::isLayoutBuilderAdminInheritedEnabled() && MdropSuiteModalHelper::isLayoutBuilderEnabled()) {
      if (!empty($ajax_page_state['theme']) && $ajax_page_state['theme'] != $this->configFactory->get('system.theme')->get('admin')) {
        $applies = TRUE;
      }
    }
    return $applies;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $valid_theme = parent::determineActiveTheme($route_match);
    if (!empty($valid_theme)) {
      return $this->configFactory->get('system.theme')->get('admin');
    }
  }

}
