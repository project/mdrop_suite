<?php

namespace Drupal\mdrop_suite_layout\Plugin\Layout;

/**
 * Configurable four column layout plugin class.
 */
class MdropSuiteLayoutFourCols extends MdropSuiteLayoutBase {

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '3-3-3-3' => '25%/25%/25%/25%',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultWidth() {
    return '3-3-3-3';
  }

}
