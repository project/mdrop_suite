<?php

namespace Drupal\mdrop_suite_layout\Plugin\Layout;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\media\MediaInterface;
use Drupal\Core\Template\Attribute;


/**
 * Class MdropSuiteLayoutBase.
 */
abstract class MdropSuiteLayoutBase extends LayoutDefault implements PluginFormInterface, ContainerFactoryPluginInterface {

  protected $entityTypeManager;
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  abstract protected function getWidthOptions();

  abstract protected function getDefaultWidth();

  protected function getUtilityClassesSettings() {
    return $this->configFactory->get('mdrop_suite_utility_classes.settings');
  }

  protected function getSpacers() {
    $spacers_count = $this->getUtilityClassesSettings()->get('spacers_count');
    $spacers = [];
    for ($spacers_loop = 0; $spacers_loop <= $spacers_count; $spacers_loop++) {
      $spacers[$spacers_loop] = $spacers_loop;
    }
    return $spacers;
  }

  protected function getGutterOptions() {
    $gutters = [];
    $gutter_class_base = $this->getUtilityClassesSettings()->get('spacers_gutter_class_base');
    foreach ($this->getSpacers() as $spacer_key) {
      $gutters[$gutter_class_base . '-' . $spacer_key] = $spacer_key == 0 ? $this->t('No gutter') : $this->t('Gutter: @spacer', ['@spacer' => $spacer_key]);
    }
    return $gutters;
  }

  protected function getVerticalSpacingOptions() {
    $vertical_spacing = [];
    $padding_class_base = $this->getUtilityClassesSettings()->get('spacers_padding_class_base');
    $vertical_class_base = $this->getUtilityClassesSettings()->get('spacers_y_class_base');
    foreach ($this->getSpacers() as $spacer_key) {
      $vertical_spacing[$padding_class_base . $vertical_class_base . '-' . $spacer_key] = $spacer_key == 0 ? $this->t('No spacing') : $this->t('Spacing: @spacer', ['@spacer' => $spacer_key]);
    }
    return $vertical_spacing;
  }

  protected function getVerticalAlignOptions() {
    return [
      'align-items-center' => $this->t('Center'),
      'align-items-end' => $this->t('Bottom'),
      'align-items-baseline' => $this->t('Baseline'),
    ];
  }

  protected function getOpacityOptions() {
    return [
      'opacity-75' => '75%',
      'opacity-50' => '50%',
      'opacity-25' => '25%',
    ];
  }

  protected function getTextAlignOptions() {
    return [
      'text-center' => $this->t('Center'),
      'text-end' => $this->t('Right'),
    ];
  }

  protected function getThemeColors() {
    return [
      'primary' => $this->t('Primary'),
      'secondary' => $this->t('Secondary'),
      'success' => $this->t('Success'),
      'info' => $this->t('Info'),
      'warning' => $this->t('Warning'),
      'danger' => $this->t('Danger'),
      'light' => $this->t('Light'),
      'dark' => $this->t('Dark'),
    ];
  }

  protected function getTextColorOptions() {
    $text_color = [];
    foreach ($this->getThemeColors() as $theme_color_class => $theme_color_label) {
      $text_color['text-' . $theme_color_class] = $theme_color_label;
    }
    return array_merge($text_color, [
      'text-body' => $this->t('Body'),
      'text-white' => $this->t('White'),
      'text-muted' => $this->t('Muted'),
      'text-black-50' => $this->t('Black 50'),
      'text-white-50' => $this->t('White 50'),
      'text-reset' => $this->t('Reset'),
    ]);
  }

  protected function getBgColorOptions() {
    $bg_color = [];
    foreach ($this->getThemeColors() as $theme_color_class => $theme_color_label) {
      $bg_color['bg-' . $theme_color_class] = $theme_color_label;
    }
    return array_merge($bg_color, [
      'bg-body' => $this->t('Body'),
      'bg-white' => $this->t('White'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $configuration['content_edge_to_edge'] = FALSE;
    $configuration['gutter'] = 'g-5';
    $configuration['vertical_spacing'] = 'py-5';
    $configuration['bg_edge_to_edge'] = TRUE;
    $configuration['bg_opacity'] = NULL;
    $configuration['bg_color'] = NULL;
    $configuration['text_color'] = NULL;
    $configuration['bg_media'] = NULL;
    $configuration['section_id'] = NULL;
    $configuration['column_widths'] = $this->getDefaultWidth();
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['content'] = [
      '#type' => 'details',
      '#title' => $this->t('Content'),
      '#open' => FALSE,
    ];

    $width_options = $this->getWidthOptions();
    $form['content']['column_widths'] = [
      '#type' => count($width_options) > 1 ? 'select' : 'value',
      '#title' => $this->t('Column widths'),
      '#default_value' => $this->configuration['column_widths'],
      '#options' => $this->getWidthOptions(),
      '#description' => $this->t('Choose the column widths for this layout.'),
    ];

    $form['content']['content_edge_to_edge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Edge to edge'),
      '#default_value' => $this->configuration['content_edge_to_edge'] ?? NULL,
      '#description' => $this->t('Content will be edge to edge or using full page available space.'),
    ];

    $form['content']['top_col'] = [
      '#type' => 'details',
      '#title' => $this->t('Top'),
      '#open' => FALSE,
    ];

    $form['content']['top_col']['top_text_color'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getTextColorOptions(),
      '#title' => $this->t('Text color'),
      '#default_value' => $this->configuration['top_text_color'] ?? NULL,
    ];

    $form['content']['top_col']['top_text_align'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getTextAlignOptions(),
      '#title' => $this->t('Text align'),
      '#default_value' => $this->configuration['top_text_align'] ?? NULL,
    ];

    $form['content']['main_cols'] = [
      '#type' => 'details',
      '#title' => $this->t('Main cols'),
      '#open' => FALSE,
    ];

    $form['content']['main_cols']['text_color'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getTextColorOptions(),
      '#title' => $this->t('Text color'),
      '#default_value' => $this->configuration['text_color'] ?? NULL,
    ];

    $form['content']['main_cols']['text_align'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getTextAlignOptions(),
      '#title' => $this->t('Text align'),
      '#default_value' => $this->configuration['text_align'] ?? NULL,
    ];

    $form['content']['bottom_col'] = [
      '#type' => 'details',
      '#title' => $this->t('Bottom'),
      '#open' => FALSE,
    ];

    $form['content']['bottom_col']['bottom_text_color'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getTextColorOptions(),
      '#title' => $this->t('Text color'),
      '#default_value' => $this->configuration['bottom_text_color'] ?? NULL,
    ];

    $form['content']['bottom_col']['bottom_text_align'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getTextAlignOptions(),
      '#title' => $this->t('Text align'),
      '#default_value' => $this->configuration['bottom_text_align'] ?? NULL,
    ];

    $form['content']['gutter'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getGutterOptions(),
      '#title' => $this->t('Gutter'),
      '#default_value' => $this->configuration['gutter'] ?? NULL,
    ];

    $form['content']['vertical_spacing'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getVerticalSpacingOptions(),
      '#title' => $this->t('Vertical spacing'),
      '#default_value' => $this->configuration['vertical_spacing'] ?? NULL,
    ];

    $form['content']['vertical_align'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('Default'),
      '#options' => $this->getVerticalAlignOptions(),
      '#title' => $this->t('Vertical align'),
      '#default_value' => $this->configuration['vertical_align'] ?? NULL,
    ];

    $form['bg'] = [
      '#type' => 'details',
      '#title' => $this->t('Background'),
      '#open' => FALSE,
    ];

    $form['bg']['bg_edge_to_edge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Edge to edge'),
      '#default_value' => $this->configuration['bg_edge_to_edge'] ?? NULL,
      '#description' => $this->t('Background will be edge to edge or using full page available space.'),
    ];

    $form['bg']['bg_opacity'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('None'),
      '#options' => $this->getOpacityOptions(),
      '#title' => $this->t('Background opacity'),
      '#default_value' => $this->configuration['bg_opacity'] ?? NULL,
      '#description' => $this->t('When background color & background image are used, opacity will apply just to image.'),
    ];

    $form['bg']['bg_color'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('None'),
      '#options' => $this->getBgColorOptions(),
      '#title' => $this->t('Background color'),
      '#default_value' => $this->configuration['bg_color'] ?? NULL,
    ];

    $bg_media_uuid = NULL;
    $bg_media = $this->configuration['bg_media'];
    if (!empty($bg_media)) {
      $look_up_for_media = $this->entityTypeManager->getStorage('media')->loadByProperties(['uuid' => $bg_media]);
      if (!empty($look_up_for_media) && reset($look_up_for_media) instanceof MediaInterface) {
        $bg_media_uuid = reset($look_up_for_media)->id();
      }
    }

    $form['bg']['bg_media'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['mdrop_suite_image', 'mdrop_suite_local_video'],
      '#title' => $this->t('Background image / video'),
      '#default_value' => $bg_media_uuid,
    ];

    $form['bg']['bg_media_modifier'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('None'),
      '#options' => [
        'half-width-left' => $this->t('Half width left'),
        'half-width-right' => $this->t('Half width right'),
      ],
      '#title' => $this->t('Background image / video modifier'),
      '#default_value' => $this->configuration['bg_media_modifier'] ?? NULL,
    ];


    $form['section_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Id'),
      '#description' => $this->t('Section identifier for anchor linking'),
      '#default_value' => $this->configuration['section_id'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['content_edge_to_edge'] = $form_state->getValue([
      'content',
      'content_edge_to_edge',
    ], NULL);
    $this->configuration['gutter'] = $form_state->getValue([
      'content',
      'gutter',
    ], NULL);
    $this->configuration['vertical_spacing'] = $form_state->getValue([
      'content',
      'vertical_spacing',
    ], NULL);
    $this->configuration['vertical_align'] = $form_state->getValue([
      'content',
      'vertical_align',
    ], NULL);
    $this->configuration['column_widths'] = $form_state->getValue([
      'content',
      'column_widths',
    ], NULL);
    $this->configuration['bg_edge_to_edge'] = $form_state->getValue([
      'bg',
      'bg_edge_to_edge',
    ], NULL);
    $this->configuration['bg_opacity'] = $form_state->getValue([
      'bg',
      'bg_opacity',
    ], NULL);
    $this->configuration['bg_color'] = $form_state->getValue([
      'bg',
      'bg_color',
    ], NULL);
    $this->configuration['top_text_color'] = $form_state->getValue([
      'content',
      'top_col',
      'top_text_color',
    ], NULL);
    $this->configuration['text_color'] = $form_state->getValue([
      'content',
      'main_cols',
      'text_color',
    ], NULL);
    $this->configuration['bottom_text_color'] = $form_state->getValue([
      'content',
      'bottom_col',
      'bottom_text_color',
    ], NULL);
    
    
    $this->configuration['top_text_align'] = $form_state->getValue([
      'content',
      'top_col',
      'top_text_align',
    ], NULL);
    $this->configuration['text_align'] = $form_state->getValue([
      'content',
      'main_cols',
      'text_align',
    ], NULL);
    $this->configuration['bottom_text_align'] = $form_state->getValue([
      'content',
      'bottom_col',
      'bottom_text_align',
    ], NULL);
    
    $bg_media_id = $form_state->getValue([
      'bg',
      'bg_media',
    ], NULL);
    $bg_media_uuid = NULL;
    if (!empty($bg_media_id)) {
      $bg_media = $this->entityTypeManager->getStorage('media')->load($bg_media_id);
      if ($bg_media instanceof MediaInterface) {
        $bg_media_uuid = $bg_media->uuid();
      }
    }
    $this->configuration['bg_media'] = $bg_media_uuid;
    $this->configuration['bg_media_modifier'] = $form_state->getValue([
      'bg',
      'bg_media_modifier',
    ], NULL);
    $this->configuration['section_id'] = $form_state->getValue(('section_id'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    if (empty($build['#attributes']) || !$build['#attributes'] instanceof Attribute) {
      $build['#attributes'] = new Attribute($build['#attributes'] ?? []);
    }
    $build['#attributes']->addClass('mdrop-suite-layout');
    $build['#attributes']->addClass(Html::cleanCssIdentifier($this->getPluginDefinition()->id()));
    $section_id = $this->configuration['section_id'];
    if (!empty($section_id)) {
      $build['#attributes']->setAttribute('id', $section_id);
    }
    $content_edge_to_edge = $this->configuration['content_edge_to_edge'];
    if (!$content_edge_to_edge) {
      $build['#attributes']->addClass('container');
    }
    $vertical_spacing = $this->configuration['vertical_spacing'];
    if (!empty($vertical_spacing)) {
      $build['#attributes']->addClass($vertical_spacing);
    }
    
    $build['#row_attributes'] = new Attribute(['class' => 'row']);
    $gutter = $this->configuration['gutter'];
    if (!empty($gutter)) {
      $build['#row_attributes']->addClass([$this->configuration['gutter']]);
    }
    $vertical_align = $this->configuration['vertical_align'];
    if (!empty($vertical_align)) {
      $build['#row_attributes']->addClass([$this->configuration['vertical_align']]);
    }
    
    $bg_edge_to_edge = $this->configuration['bg_edge_to_edge'] ?? NULL;

    $bg_classes = ['mdrop-suite-layout-bg-wrapper'];
    $bg_opacity = $this->configuration['bg_opacity'];
    $bg_media_modifier = $this->configuration['bg_media_modifier'] ?? NULL;
    $bg_media = $this->configuration['bg_media'];
    if (!empty($bg_opacity) && empty($bg_media)) {
      $bg_classes[] = 'bg-' . $bg_opacity;
    }
    $bg_color = $this->configuration['bg_color'];

    if (!empty($bg_color)) {
      $bg_classes[] = $bg_color;
      $build['#attributes']->addClass('has-bg-color');
    }
    if ($bg_edge_to_edge) {
      $build['#theme_wrappers']['container']['#attributes']['class'] = array_merge($build['#theme_wrappers']['container']['#attributes']['class'] ?? [], $bg_classes);
    }
    else {
      $build['#attributes']->addClass($bg_classes);
    }
    if (!empty($bg_media)) {
      $look_up_for_media = $this->entityTypeManager->getStorage('media')->loadByProperties(['uuid' => $bg_media]);
      if (!empty($look_up_for_media) && reset($look_up_for_media) instanceof MediaInterface) {
        $bg_media_media = reset($look_up_for_media);
        if ($bg_media_media instanceof MediaInterface) {
          $build['bg_media'] = ['#type' => 'container'];
          $build['bg_media']['media'] = $this->entityTypeManager->getViewBuilder('media')->view($bg_media_media, 'mdrop_suite_background');
          $build['bg_media']['#attributes']['class'][] = 'mdrop-suite-layout-bg-wrapper__bg-media';
          if (!empty($bg_opacity)) {
            $build['bg_media']['#attributes']['class'][] = $bg_opacity;
          }
          if (!empty($bg_edge_to_edge)) {
            $build['bg_media']['#attributes']['class'][] = $bg_media_modifier;
          }
        }
      }
    }
    
    $top_classes = ['col-12'];
    $top_text_color = $this->configuration['top_text_color'];
    if (!empty($top_text_color)) {
      $top_classes[] = $top_text_color;
    }
    $top_text_align = $this->configuration['top_text_align'];
    if (!empty($top_text_align)) {
      $top_classes[] = $top_text_align;
    }
    $bottom_classes = ['col-12'];
    $bottom_text_color = $this->configuration['bottom_text_color'];
    if (!empty($bottom_text_color)) {
      $bottom_classes[] = $bottom_text_color;
    }
    $bottom_text_align = $this->configuration['bottom_text_align'];
    if (!empty($bottom_text_align)) {
      $bottom_classes[] = $bottom_text_align;
    }
    $main_class = ['col-12'];
    $text_color = $this->configuration['text_color'];
    if (!empty($text_color)) {
      $main_class[] = $text_color;
    }
    $text_align = $this->configuration['text_align'];
    if (!empty($text_align)) {
      $main_class[] = $text_align;
    }
    $column_widths = $this->configuration['column_widths'];
    $column_widths_items = explode('-', $column_widths);
    $auto = in_array('auto', $column_widths_items);
    foreach ($this->getPluginDefinition()->getRegionNames() as $region_name) {
      if ($region_name == 'top') {
        $build['#' . $region_name . '_class'] = $top_classes;
      }
      elseif ($region_name == 'bottom') {
        $build['#' . $region_name . '_class'] = $bottom_classes;
      }
      else {
        $build['#' . $region_name . '_class'] = array_merge($main_class, [
          'col-md-' . array_shift($column_widths_items),
          $auto ? 'mx-auto' : NULL,
        ]);
        // Make sure sections initialized when empty to avoid grid to fail when
        // using empty regions combined with background image for example.
        if (empty($build[$region_name])) {
          $build[$region_name] = [];
        }
      }
    }
    
    
    $build['#attached']['library'][] = 'mdrop_suite_layout/layout';

    return $build;
  }

}
