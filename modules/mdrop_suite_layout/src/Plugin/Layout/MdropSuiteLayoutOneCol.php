<?php

namespace Drupal\mdrop_suite_layout\Plugin\Layout;

/**
 * Configurable two column layout plugin class.
 */
class MdropSuiteLayoutOneCol extends MdropSuiteLayoutBase {

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '12' => '100%',
      '9-auto' => '75%',
      '6-auto' => '50%',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultWidth() {
    return '12';
  }

}
