<?php

namespace Drupal\mdrop_suite_layout\Plugin\Layout;

/**
 * Configurable three column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class MdropSuiteLayoutThreeCols extends MdropSuiteLayoutBase {

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '3-6-3' => '25%/50%/25%',
      '4-4-4' => '33%/34%/33%',
      '3-3-6' => '25%/25%/50%',
      '6-3-3' => '50%/25%/25%',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultWidth() {
    return '4-4-4';
  }

}
