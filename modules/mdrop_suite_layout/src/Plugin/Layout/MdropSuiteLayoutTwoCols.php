<?php

namespace Drupal\mdrop_suite_layout\Plugin\Layout;

/**
 * Configurable two column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class MdropSuiteLayoutTwoCols extends MdropSuiteLayoutBase {

  const COLS_EQUAL = 6;

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '6-6' => '50%/50%',
      '4-8' => '33%/67%',
      '8-4' => '67%/33%',
      '3-9' => '25%/75%',
      '9-3' => '75%/25%',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultWidth() {
    return '6-6';
  }

}
