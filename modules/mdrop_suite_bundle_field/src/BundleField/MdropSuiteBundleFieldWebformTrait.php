<?php

namespace Drupal\mdrop_suite_bundle_field\BundleField;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\BundleFieldDefinition;

trait MdropSuiteBundleFieldWebformTrait {

  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {

    $fields['mdrop_suite_webform'] = BundleFieldDefinition::create('webform')
      ->setName('mdrop_suite_webform')
      ->setLabel(t('Webform'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setCardinality(1)
      // @code ->setRevisionable(TRUE) @endcode
      ->setSetting('handler', 'default')
      // Display.
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'webform_entity_reference_entity_view',
        'settings' => [
          'source_entity' => FALSE,
        ],
      ])
      // Form display.
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'webform_entity_reference_select',
        'settings' => [
          'default_data' => FALSE,
        ],
      ]);
    return $fields;
  }

}
