<?php

namespace Drupal\mdrop_suite_bundle_field\BundleField;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\BundleFieldDefinition;

trait MdropSuiteBundleFieldParagraphTrait {

  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {

    $fields['mdrop_suite_paragraph'] = BundleFieldDefinition::create('entity_reference_revisions')
      ->setName('mdrop_suite_paragraph')
      ->setLabel(t('Paragraph'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setCardinality(1)
      // @code ->setRevisionable(TRUE) @endcode
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler', 'default')
      // Display.
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_revisions_entity_view',
      ])
      // Form display.
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'paragraphs',
        'settings' => [
          'edit_mode' => 'open',
          'add_mode' => 'button',
          'features' => [
            'duplicate' => FALSE,
            'collapse_edit_all' => FALSE,
            'add_above' => FALSE,
          ],
        ],
      ]);
    return $fields;
  }

}
