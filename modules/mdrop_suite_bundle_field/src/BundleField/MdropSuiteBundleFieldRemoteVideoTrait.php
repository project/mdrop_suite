<?php

namespace Drupal\mdrop_suite_bundle_field\BundleField;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\BundleFieldDefinition;

trait MdropSuiteBundleFieldRemoteVideoTrait {

  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {

    $fields['mdrop_suite_remote_video'] = BundleFieldDefinition::create('entity_reference')
      ->setName('mdrop_suite_remote_video')
      ->setLabel(t('Remote video'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setCardinality(1)
      // @code ->setRevisionable(TRUE) @endcode
      ->setSetting('target_type', 'media')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'mdrop_suite_remote_video' => 'mdrop_suite_remote_video',
        ],
      ])
      // Display.
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => ['view_mode' => 'default'],
        'label' => 'hidden',
      ])
      // Form display.
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'media_library_widget',
        'region' => 'content',
        'settings' => [
          'media_types' => ['mdrop_suite_remote_video'],
        ],
      ]);
    return $fields;
  }

}
