<?php

namespace Drupal\mdrop_suite_bundle_field\BundleField;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\BundleFieldDefinition;

trait MdropSuiteBundleFieldImageTrait {

  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {

    $fields['mdrop_suite_image'] = BundleFieldDefinition::create('entity_reference')
      ->setName('mdrop_suite_image')
      ->setLabel(t('Image'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setCardinality(1)
      // @code ->setRevisionable(TRUE) @endcode
      ->setSetting('target_type', 'media')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'mdrop_suite_image' => 'mdrop_suite_image',
        ],
      ])
      // Display.
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'media_responsive_thumbnail',
        'settings' => ['responsive_image_style' => 'mdrop_suite_image_original_optimized'],
        'label' => 'hidden',
      ])
      // Form display.
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'media_library_widget',
        'region' => 'content',
        'settings' => [
          'media_types' => ['mdrop_suite_image'],
        ],
      ]);
    return $fields;
  }

}
