<?php

namespace Drupal\mdrop_suite_bundle_field;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Helper "MdropSuiteBundleFieldHelper" service object.
 */
class MdropSuiteBundleFieldHelper {

  protected $entityDefinitionsUpdateManager;

  protected $entityTypeManager;

  /**
   * Constructs a "MdropSuiteMdropSuiteBundleFieldsHelper" object.
   */
  public function __construct(EntityDefinitionUpdateManagerInterface $entity_definitions_update_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityDefinitionsUpdateManager = $entity_definitions_update_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  public function installBlockContentBundleFields($bundle_key, $class) {
    $this->installEntityTypeBundleFields('block_content', $bundle_key, $class);
  }

  private function installEntityTypeBundleFields($entity_type_key, $bundle_key, $class) {
    $entity_type = $this->entityTypeManager->getDefinition('block_content');
    $bundle_field_definitions = $entity_type instanceof EntityTypeInterface ? $class::bundleFieldDefinitions($entity_type, $bundle_key, []) : [];
    if (!empty($bundle_field_definitions)) {
      foreach ($bundle_field_definitions as $bundle_field_definition) {
        $this->entityDefinitionsUpdateManager->installFieldStorageDefinition($bundle_field_definition->getName(), $entity_type_key, 'mdrop_suite', $bundle_field_definition);
      }
    }
    // In other cases may be useful:
    // @code $field_storage_defs = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node'); @code
    // @code $this->entityDefinitionsUpdateManager->installFieldableEntityType($entity_type, $field_storage_defs); @code
    // @code $this->entityDefinitionsUpdateManager->updateFieldableEntityType($entity_type, $field_storage_defs); @code
    // @code $this->entityDefinitionsUpdateManager->updateEntityType($entity_type); @endcode
    // @code $this->entityDefinitionsUpdateManager->getChangeList($entity_type); @endcode
    // @code $this->entityDefinitionsUpdateManager->uninstallFieldStorageDefinition($field_storage_defs['deprecatedfield']); @endcode
  }

}
