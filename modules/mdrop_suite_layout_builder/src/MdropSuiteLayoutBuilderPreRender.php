<?php

namespace Drupal\mdrop_suite_layout_builder;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\Url;

/**
 * Layout builder pre-render class.
 */
class MdropSuiteLayoutBuilderPreRender implements TrustedCallbackInterface {

  /**
   * Layout builder pre-render callback.
   *
   * @param array $element
   *   Render element.
   *
   * @return array
   *   Element modified.
   */
  public static function preRender(array $element) {
    $element['#attached']['library'][] = 'mdrop_suite_layout_builder/layout_builder';
    
    foreach ($element['layout_builder'] as &$child_element) {
      // Section library changes this key?!
      $section_key = !empty($child_element[0]['#layout']) ? 0 : (!empty($child_element['layout-builder__section']['#layout']) ? 'layout-builder__section' : NULL);
      if (($section_key === 0 || !empty($section_key)) && $child_element[$section_key]['#layout'] instanceof LayoutDefinition) {
        $child_element['section_main_actions'] = [];
        
        if (!empty($child_element['configure'])) {
          $child_element['section_main_actions']['configure'] = $child_element['configure'];
          unset($child_element['configure']);
        }
        if (!empty($child_element['add_to_library'])) {
          $child_element['section_main_actions']['add_to_library'] = $child_element['add_to_library'];
          unset($child_element['add_to_library']);
        }
        if (!empty($child_element['remove'])) {
          $child_element['section_main_actions']['remove'] = $child_element['remove'];
          unset($child_element['remove']);
        }
        if (!empty($child_element['section_main_actions'])) {
          $child_element['section_main_actions'] += [
            '#type' => 'container',
            '#attributes' => ['class' => ['container', 'layout-builder__section__main-actions']],
            '#weight' => -1,
          ];
        }
        foreach ($child_element[$section_key]['#layout']->getRegions() as $region => $info) {
          if (empty($child_element[$section_key][$region])) {
            continue;
          }
          foreach ($child_element[$section_key][$region] as &$section_child_element) {
            if (isset($section_child_element['#theme']) && $section_child_element['#theme'] === 'block') {
              continue 2;
            }
          }
          $child_element[$section_key][$region]['#attributes']['class'][] = 'layout-builder__region--empty-region';
        }
      }
    }
    // @todo empty top/bottom regions spacing, add class to remove padding.
    $element['live_preview'] = [
      '#type' => 'link',
      '#attributes' => [
        'class' => ['layout-builder__live-preview__toggler__link'],
        'title' => t('Live preview'),
      ],
      '#title' => t('Live preview'),
      '#url' => Url::fromUserInput('#live-preview'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'preRender',
    ];
  }

}
