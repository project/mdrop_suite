(function (Drupal, once, window) {
  Drupal.behaviors.mdrop_suite_layout_builder = {
    attach(context) {
      once('layout-builder-live-preview', '.layout-builder__live-preview__toggler__link', context).forEach(layoutBuilderLivePreview);
    }
  };

  /**
   * Layout builder live preview.
   */
  function layoutBuilderLivePreview(toggler) {
    toggler.addEventListener('click', function (e) {
      e.preventDefault();
      document.querySelector('.layout-builder').classList.toggle('live-priew-active');
    });
  }

}(Drupal, once, window));
