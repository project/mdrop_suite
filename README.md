# EN

## Description

To get complete suite from start; with blocks, dedicated landing using layout
builder & content editor role, just enable mdrop_suite_landing_content_editor.
Use with Radix or any other Bootstrap 5 based theme for now, or you can
implement customizable used helper classes in your theme.

After installation adjust all configuration to your project mainly:
 - Text format.
 - Role permissions.
 - Views allowed to be used into landing display.
 - Customize the content page.html without a "container" class to ge edge to
 edge working as expected e.g: "page--node--mdrop-suite-landing.html" &
 "page--admin--structure--types--manage--mdrop-suite-landing.html".

To get a better UX also recommended admin_toolbar & ajax_loader with always
fullscreen loader option.

Some components require additional modules like paragraphs or webform for
block integration (not required in composer by default).

 Still WIP!!

## Installation

Ensure listed patches in composer with "cweagans/composer-patches".

Just enable any included module of your choice or all.
